## TAREA PSP 01 - Ejercicio 1

## Instalación

Clonar el repositorio:

```
git clone git@bitbucket.org:paperezsi/psp2018-tarea01.git
git checkout ejercicio01
```




##Pasos para probar la ejecución del ejercicio 1.

Para la resolución de la tarea, inicialmente creo dos proyectos Java utilizando Maven.

Para realizar la comprobación de la aplicación, así como la redirección de la salida de "aleatorios" a la entrada de "ordenarNumeros" he seguido los siguientes pasos:

 * **PASO 1:** El primer paso ha sido desarrollar las aplicaciones y asegurarse de que cada aplicación realiza correctamente su función.

![paso00.png](readme_src/img/paso00.png)
 

* **PASO 2:** A continuación se compilan las dos aplicaciones para obtener los fichero .jar que serán los que finalmente se ejecutarán. Para ello con el botón secundario pulsamos sobre el proyecto y pulsamos la opción de exportar, marcamos la opción de Runnable JAR file y presionamos en continuar.

![paso01.png](readme_src/img/paso01.png)

 - En la siguiente ventana debemos seleccionar la aplicación que queremos compilar y la ruta de destino del fichero .jar, en mi caso estos ficheros así como las librerías necesarias se encuentran en un directorio llamado "ficherosJAR", dentro de la carpeta raíz del proyecto.

![paso02.png](readme_src/img/paso02.png)

![paso03.png](readme_src/img/paso03.png)
 
 - Tras repetir los pasos anteriores con la aplicación "colaborar", tendremos en el directorio "ficherosJAR" todo lo necesario para probar la ejecución de las dos aplicaciones.

![paso04.png](readme_src/img/paso04.png)

* **PASO 3:** Situado en el directorio donde tenemos los .jar, ejecutamos una nueva ventana de la consola del sistema, en este caso, PowerShell, en la linea de comandos introduccimos la siguiente instrucción:

```
 java -jar aleatorios.jar | java -jar ordenarnumeros.jar
```

> Nota: Es importante destacar el operador " | " entre la ejecución de las dos aplicaciones, con el conseguimos que la salida estándar de la primera aplicación ( "aleatorios.jar" ) se convierta en la estrada estándar de la segunda ("ordenarnumeros.jar"). Este operador nos permite comunicar dos procesos haciendo uso de las tuberías del sistema operativo.

![paso05.png](readme_src/img/paso05.png)

![paso06.png](readme_src/img/paso06.png)



* **RESULTADO FINAL:** Podemos comprobar el resultado de la ejecución, aparecerán los números aleatorios creados por el primer proceso, ordenados por el segundo proceso.

![paso07.png](readme_src/readme_src/img/paso07.png)



## Meta

Pablo Pérez – ue57919@edu.xunta.es

Distribuido bajo la licencia CC BY. Ver [``LICENCIAS``](https://creativecommons.org/licenses/?lang=es_ES) para más información.

[https://bitbucket.org/paperezsi/](https://bitbucket.org/paperezsi/)